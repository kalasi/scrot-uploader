[![license-badge][]][license]

# scrot-uploader

A small wrapper around `scrot` to upload the resulting scrot file to a given
URL (the first argument). All other arguments are passed to scrot.

### Examples

Take a screenshot of your entire screen:

```sh
scrot-uploader WEBSITE_URL
```

Take a screenshot of a single window:

```sh
scrot-uploader WEBSITE_URL -s
```

The `-s` argument is a scrot argument. Read the scrot manpage for more info.

### Installation

1. `git clone https://gitlab.com/kalasi/scrot-uploader.git`
2. `make`

### Flags

- `-h|--help` View the help
- `-n|--no-copy` Do not copy the URL to the file to the clipboard

### License

See the LICENSE.md file. Basically, UNLICENSE.

[license-badge]: https://img.shields.io/badge/license-UNLICENSE-lightgrey.svg?style=flat-square
[license]: http://unlicense.org/
