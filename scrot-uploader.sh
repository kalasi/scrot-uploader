#!/usr/bin/env bash
#
# Originally by zeyla.
#
# LICENSE:
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org>

# If there are no arguments, then let the user know there needs to be at least
# one argument for the URL to upload to.
if [[ $# -eq 0 ]]; then
    echo "The first argument should be the URL to upload to."
    exit 0
fi

UPLOAD_URL="$1"

# Call scrot and pass all but the first argument to it.
scrot "${@:2}"

# Get the most recent file (should be the scrot image)
FILENAME=`ls -td * | head -1`

# Upload to the filehost.
OUT=`curl -i -F file="@$FILENAME" "$1"`

URL=$(echo "$OUT" | tail -n 1)

# Remove the temporary scrot file.
rm "$FILENAME"

# Echo the resulting contents (probably a URL) to the stdout.
echo "$URL"

# Copy the URL to the clipboard if set to.
echo "$URL" | xsel --clipboard
